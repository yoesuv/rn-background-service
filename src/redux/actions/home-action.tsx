import { ActionCreator } from "redux";
import { HomeActionType, HOME_APP_START, HOME_APP_STOP } from "../types";

export const homeAppStart: ActionCreator<HomeActionType> = () => {
    return {
        type: HOME_APP_START,
    }
}

export const homeAppStop: ActionCreator<HomeActionType> = () => {
    return {
        type: HOME_APP_STOP,
    }
}