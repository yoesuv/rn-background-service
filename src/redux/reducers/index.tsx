import { combineReducers } from 'redux';
import { homeReducers } from './home-reducer';

export const rootReducer = combineReducers({
    home: homeReducers
});

export type RootState = ReturnType<typeof rootReducer>;