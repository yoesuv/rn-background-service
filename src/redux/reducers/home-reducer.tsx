import { HOME_APP_START, HOME_APP_STOP } from '../types';

interface Home {
    type: String,
}

const initialState = {
    isStart: false
}

export function homeReducers(state = initialState, action: Home) {
    switch (action.type) {
        case HOME_APP_START:
            return {
                ...state,
                isStart: true,
            }
        case HOME_APP_STOP: 
            return {
                ...state,
                isStart: false,
            }
        default:
            return state;
    }
}