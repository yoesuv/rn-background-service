export const HOME_APP_START = "HOME_APP_START";
export const HOME_APP_STOP = "HOME_APP_STOP";

interface homeAppStart {
    type: typeof HOME_APP_START;
}

interface homeAppStop {
    type: typeof HOME_APP_STOP;
}

export type HomeActionType = homeAppStart | homeAppStop;