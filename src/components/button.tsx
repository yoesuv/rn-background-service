import React from 'react';
import { Text, StyleSheet, Pressable, GestureResponderEvent } from 'react-native';

interface ButtonProps {
  onPress: (event: GestureResponderEvent) => void;
  title?: String;
  disabled?: boolean,
}

const AppButton: React.FC<ButtonProps> = ({onPress, title = 'Save', disabled = false}) => {
  return <Pressable disabled={disabled} style={({ pressed }) => [
      disabled ? styles.buttonDisabled : pressed ? styles.buttonPress :styles.button
    ]} onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
  </Pressable>
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 8,
    backgroundColor: '#009688',
    flex: 1,
  },
  buttonPress: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 8,
    backgroundColor: '#00796B',
    flex: 1,
  },
  buttonDisabled: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 8,
    backgroundColor: '#BDBDBD',
    flex: 1,
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
});

export default AppButton;
