import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator, NativeStackNavigationOptions } from '@react-navigation/native-stack';
import HomeScreen from './screens/home';
import { RootStackParamList } from './screens/root-stack-params';
import SplashScreen from './screens/splash';

const Stack = createNativeStackNavigator<RootStackParamList>();

const baseOptions: NativeStackNavigationOptions = {
    title: "RN Load More",
    headerStyle: {
      backgroundColor: '#009688'
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    statusBarColor: '#009688',
  };

const AppNavigation = () => {
    return <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name='Splash' component={SplashScreen} options={{headerShown: false}}/>
            <Stack.Screen name='Home' component={HomeScreen} options={{...baseOptions, title: 'Home'}}/>
        </Stack.Navigator>
    </NavigationContainer>
}

const AppContainer = () => {
    return <AppNavigation />
}

export default AppContainer;