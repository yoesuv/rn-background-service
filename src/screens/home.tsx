import { StyleSheet, View, Text } from "react-native";
import { useDispatch, useSelector } from 'react-redux';

import AppButton from "../components/button";
import SizedBox from "../components/sized-box";
import { homeAppStart, homeAppStop } from "../redux/actions";
import { RootState } from "../redux/reducers";

export default function HomeScreen() {
    
    const dispatch = useDispatch();
    const stateHome = useSelector((state: RootState) => state.home);
    const isStart = stateHome.isStart;

    return <View style={styles.container}>
        <SizedBox height={10} />
        <Text style={styles.label}>Background Service Status:</Text>
        <SizedBox height={10} />
        <Text style={styles.labelBold}>[STATUS : {isStart.toString()}]</Text>
        <SizedBox height={10} />
        <View style={styles.button}>
            <AppButton  title={'START'} onPress={() => {
                dispatch(homeAppStart());
            }} />
        </View>
        <SizedBox height={10} />
        <View style={styles.button}>
            <AppButton title={'STOP'} onPress={() => {
                dispatch(homeAppStop());
            }} />
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 24,
        paddingVertical: 16,
    },
    button: {
        height: 50,
    },
    label: {
        fontSize: 16,
        textAlign: 'center',
    },
    labelBold: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});