import { Provider } from 'react-redux';

import AppContainer from './src/navigation';
import store from './src/redux/store'

const configureStore = store();

export default function App() {
  return (
    <Provider store={configureStore}>
      <AppContainer />
    </Provider>
  );
}
